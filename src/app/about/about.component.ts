import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  // Count of features
  user_count: number = 1;
  course_count: number = 1;
  event_count: number = 1;

  constructor() { }

  ngOnInit(): void {
  }

}
