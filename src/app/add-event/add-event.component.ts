import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EventService } from '../event.service';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss']
})
export class AddEventComponent implements OnInit {
  reader = new FileReader();
  file: any;
  name = 'Description';
  htmlContent = '';

  RegisterForm: FormGroup = this.fb.group(
    {
      name: ['', Validators.required],
      organizer: ['', Validators.required],
      description: ['', Validators.required],
      fee: ['', Validators.required],
      duration: ['', Validators.required],
      contact: ['', Validators.required],
    });

  constructor(private api: EventService,
    private fb: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }


  fileChangeEvent(e: any) {

    this.file = e.target.files[0];
    this.reader.readAsDataURL(this.file);
    this.reader.onload = (_event) => {
      this.file = this.reader.result;
      console.log(this.file);
    }
  }

  checkDetails() {
    let form = this.RegisterForm.value;
    console.log(form);
    form.image_url = this.file

    this.api.addEvent(form)
      .subscribe(
        (response: any) => {
          let result = response;
          console.log(result);
        })
    this.router.navigate(['admin/event']);
  }

}