import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  AUTH_URL = "/api/api/login";

  constructor(private _http: HttpClient, private router: Router) {
  }

  loginUser(data: any) {
    return this._http.post(this.AUTH_URL, data);
  }
  loggedIn() {
    return !!localStorage.getItem('token')
  }

  getToken() {
    return localStorage.getItem('token')
  }
}
