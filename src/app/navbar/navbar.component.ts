import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isCollapsed = true;
  isAuth = localStorage.getItem('role') ? true : false;
  constructor() { }

  ngOnInit(): void {

  }
  responsiveDropdown() {
    this.isCollapsed = !this.isCollapsed;
    $("#arrowToggle").toggleClass("fa-arrow-down fa-arrow-up");
  }

}
