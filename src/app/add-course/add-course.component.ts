import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.scss'],
})
export class AddCourseComponent implements OnInit {
  reader = new FileReader();
  file: any;
  name = 'Description';
  htmlContent = '';

  RegisterForm: FormGroup = this.fb.group(
    {
      title: ['', Validators.required],
      category: ['', Validators.required],
      description: ['', Validators.required],
      criteria: ['', Validators.required],
      fee: ['', Validators.required],
      duration: ['', Validators.required],
      internship_partner: ['', Validators.required],
    });

  constructor(private api: CourseService,
    private fb: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }


  fileChangeEvent(e: any) {

    this.file = e.target.files[0];
    this.reader.readAsDataURL(this.file);
    this.reader.onload = (_event) => {
      this.file = this.reader.result;
      console.log(this.file);
    }
  }

  checkDetails() {
    let form = this.RegisterForm.value;
    form.image_url = this.file

    this.api.addCourse(form)
      .subscribe(
        (response: any) => {
          let result = response;
          console.log(result);
        })
    this.router.navigate(['']);
  }

}
