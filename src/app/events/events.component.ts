import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';
import * as $ from "jquery";

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  events: any;
  constructor(private api: EventService) { }

  ngOnInit(): void {
    this.api.listEvents().subscribe(
      (resp: any) => {
        console.log(resp)
        this.events = resp;
      }
    )
  }

  expandText(id: string) {
    $(`#${id}`).css({
      'height': 'auto'
    });
  }
}
