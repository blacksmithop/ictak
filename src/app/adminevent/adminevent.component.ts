import { Component, OnInit } from '@angular/core';
import { AdminviewService } from '../adminview.service';
import { EventService } from '../event.service';

@Component({
  selector: 'app-adminevent',
  templateUrl: './adminevent.component.html',
  styleUrls: ['./adminevent.component.scss']
})
export class AdmineventComponent implements OnInit {

  registeredEvents: any;
  listEvents: any;
  constructor(private api: AdminviewService,
    private event: EventService) {
  }

  ngOnInit(): void {

    this.event.listEvents().subscribe(
      (resp: any) => {
        console.log(resp)
        this.listEvents = resp;
      });

    this.api.listRegisteredEvents().subscribe(
      (resp: any) => {
        console.log(resp)
        this.registeredEvents = resp;
      }
    );
  }

}
