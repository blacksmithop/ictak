import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private fb: FormBuilder,
    private login: AuthService,
    private router: Router) { }

  ngOnInit(): void {
    /*
    if (this.login.loggedIn()) {
      this.router.navigate(['home']);
    }*/
  }
  LoginForm: FormGroup = this.fb.group(
    {
      username: ['', Validators.required],
      password: ['', Validators.required]
    }
  )

  checkCredentials() {
    this.login.loginUser(this.LoginForm.value)
      .subscribe(
        (response: any) => {
          let result = response;
          console.log(result);
          if ('data' in response) {
            localStorage.setItem('token', response.token)
            localStorage.setItem('role', response.data.isAdmin)
            localStorage.setItem('user', response.data)
            if (response.data.isAdmin == true) {
              this.router.navigate(['admin/course']);
            }
            else {
              this.router.navigate(['']);
            }
            return response.data;
          } else {
            alert('User not found')
          }
        })
  }


}