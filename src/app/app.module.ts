import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Services
import { AuthService } from './auth.service';
import { CourseService } from './course.service';
import { TokenInterceptorService } from './token-interceptor.service';
import { EventService } from './event.service';

//Components
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BodyComponent } from './body/body.component';
import { RegisterCourseComponent } from './register-course/register-course.component';
import { ListComponent } from './list/list.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { AboutComponent } from './about/about.component';
import { EventsComponent } from './events/events.component';
import { CoursesComponent } from './courses/courses.component';
import { ApproverComponent } from './approver/approver.component';
//404 component
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
// ngx
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { AdminDashComponent } from './admin-dash/admin-dash.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AdminNavComponent } from './admin-nav/admin-nav.component';
import { ContactComponent } from './contact/contact.component';
import { RegisterEventComponent } from './register-event/register-event.component';
import { AddCourseComponent } from './add-course/add-course.component';
// rich text
import { AngularEditorModule } from '@kolkov/angular-editor';

import { EditCourseComponent } from './edit-course/edit-course.component';
import { AdminCourseComponent } from './admin-course/admin-course.component';
import { LogoutComponent } from './logout/logout.component';
import { AdmineventComponent } from './adminevent/adminevent.component';
import { DeleteCourseComponent } from './delete-course/delete-course.component';
import { DeleteCourseRegComponent } from './delete-course-reg/delete-course-reg.component';
import { DeleteEventComponent } from './delete-event/delete-event.component';
import { DeleteEventRegComponent } from './delete-event-reg/delete-event-reg.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import { AddEventComponent } from './add-event/add-event.component';
import { PartnerComponent } from './partner/partner.component';
import { AdminPartnerComponent } from './admin-partner/admin-partner.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BodyComponent,
    RegisterCourseComponent,
    ListComponent,
    FooterComponent,
    LoginComponent,
    AboutComponent,
    EventsComponent,
    CoursesComponent,
    ApproverComponent,
    NotFoundPageComponent,
    AdminDashComponent,
    AdminNavComponent,
    ContactComponent,
    RegisterEventComponent,
    AddCourseComponent,
    EditCourseComponent,
    AdminCourseComponent,
    LogoutComponent,
    AdmineventComponent,
    DeleteCourseComponent,
    DeleteCourseRegComponent,
    DeleteEventComponent,
    DeleteEventRegComponent,
    EditEventComponent,
    AddEventComponent,
    PartnerComponent,
    AdminPartnerComponent,
  ],
  imports: [
    BrowserModule,
    AngularEditorModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ButtonsModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
  ],
  providers: [AuthService, CourseService, EventService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
