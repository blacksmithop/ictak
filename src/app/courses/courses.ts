const course = [
    {
        name: "CERTIFIED SPECIALIST IN FULL STACK DEVELOPMENT",
        about: "The MERN stack is an excellent choice for web developers who wish to develop high-quality web applications using JavaScript. The core technologies define the MERN stack – MongoDB, Express.js, React, and Node.js – all are based on one language, Javascript",
        duration: 6,
        price: 100000,
        internship: true,
        scholarship: true
    },
    {
        name: "CERTIFIED SPECIALIST IN FULL STACK DEVELOPMENT",
        about: "The MERN stack is an excellent choice for web developers who wish to develop high-quality web applications using JavaScript. The core technologies define the MERN stack – MongoDB, Express.js, React, and Node.js – all are based on one language, Javascript",
        duration: 6,
        price: 100000,
        internship: true,
        scholarship: true
    },
    {
        name: "CERTIFIED SPECIALIST IN DATA SCIENCE & ANALYTICS",
        about: "Data Science and Analytics are amongst the Top 5 IT Jobs . One of the worlds famous tech group, CIO.com predicts that the skills around Data Science will remain as popular for the next few years.",
        duration: 8,
        price: 120000,
        internship: false,
        scholarship: true
    },
    {
        name: "ROBOTIC PROCESS AUTOMATION",
        about: "Robotic process automation (RPA) is the use of software with artificial intelligence (AI) and machine learning capabilities to handle high-volume, repeatable tasks that previously required humans to perform.",
        duration: 8,
        price: 80000,
        internship: true,
        scholarship: false
    },
]