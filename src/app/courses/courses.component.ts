import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';
import * as $ from "jquery";

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  courses: any;
  constructor(private api: CourseService) { }

  ngOnInit(): void {
    this.api.listCourses().subscribe(
      (resp: any) => {
        console.log(resp)
        this.courses = resp;
      }
    )
  }

  expandText(id: string) {
    $(`#${id}`).css({
      'height': 'auto'
    });
  }

}
