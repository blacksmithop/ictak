import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteCourseRegComponent } from './delete-course-reg.component';

describe('DeleteCourseRegComponent', () => {
  let component: DeleteCourseRegComponent;
  let fixture: ComponentFixture<DeleteCourseRegComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteCourseRegComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCourseRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
