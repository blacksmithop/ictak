import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CourseService } from '../course.service';

@Component({
  selector: 'app-delete-course-reg',
  templateUrl: './delete-course-reg.component.html',
  styleUrls: ['./delete-course-reg.component.scss']
})
export class DeleteCourseRegComponent implements OnInit {

  constructor(private api: CourseService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.api.deleteCourseReg(this.route.snapshot.params.courseid).subscribe(
      (response: any) => {
        let result = response;
        console.log(result);
      })
    this.router.navigate(['admin/course']);
  }

}

