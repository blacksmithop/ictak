import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CourseService } from '../course.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-register-event',
  templateUrl: './register-event.component.html',
  styleUrls: ['./register-event.component.scss']
})
export class RegisterEventComponent implements OnInit {
  RegisterForm: FormGroup = this.fb.group(
    {
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required]

    }
  )
  constructor(private fb: FormBuilder,
    private api: CourseService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

  checkDetails() {
    let form = this.RegisterForm.value;
    form['course_id'] = this.route.snapshot.params.courseid;
    console.log(form);
    this.api.registerCourse(form)
      .subscribe(
        (response: any) => {
          let result = response;
          console.log(result);
        })
    this.router.navigate(['']);
  }
}
