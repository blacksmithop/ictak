import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteEventRegComponent } from './delete-event-reg.component';

describe('DeleteEventRegComponent', () => {
  let component: DeleteEventRegComponent;
  let fixture: ComponentFixture<DeleteEventRegComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteEventRegComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteEventRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
