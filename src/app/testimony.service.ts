import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TestimonyService {
  EVENT_URL = "/api/api/testimony/"
  constructor(private _http: HttpClient) { }

  listTestimony() {
    return this._http.get(`${this.EVENT_URL}/list`)
  }

  registerTestimony(data: any) {
    return this._http.post(`${this.EVENT_URL}/add`, data)

  }
}