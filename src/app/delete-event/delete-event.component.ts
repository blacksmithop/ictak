import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../event.service';

@Component({
  selector: 'app-delete-event',
  templateUrl: './delete-event.component.html',
  styleUrls: ['./delete-event.component.scss']
})
export class DeleteEventComponent implements OnInit {


  constructor(private api: EventService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.api.deleteEvent(this.route.snapshot.params.courseid).subscribe(
      (response: any) => {
        let result = response;
        console.log(result);
      })
    this.router.navigate(['admin/event']);
  }

}
