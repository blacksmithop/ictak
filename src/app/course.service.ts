import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  COURSE_URL = "/api/api/course/"
  constructor(private _http: HttpClient) { }

  listCourses() {
    return this._http.get(`${this.COURSE_URL}/list`)
  }

  registerCourse(data: any) {
    return this._http.post(`${this.COURSE_URL}/register`, data)
  }

  getCourse(id: any) {
    return this._http.get(`${this.COURSE_URL}/get/${id}`)
  }
  deleteCourse(id: any) {
    return this._http.get(`${this.COURSE_URL}/remove/${id}`)
  }

  deleteCourseReg(id: any) {
    return this._http.get(`${this.COURSE_URL}/removereg/${id}`)
  }



  addCourse(data: any) {
    return this._http.post(`${this.COURSE_URL}/add`, data)
  }

  editCourse(data: any) {
    return this._http.post(`${this.COURSE_URL}/update`, data)
  }
}
