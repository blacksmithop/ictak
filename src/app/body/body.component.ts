import { Component, OnInit } from '@angular/core';
import { TestimonyService } from '../testimony.service';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit {
  data: any;
  constructor(private api: TestimonyService) { }

  ngOnInit(): void {
    this.api.listTestimony().subscribe(
      (resp: any) => {
        this.data = resp;
      }
    )
  }

}
