import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminviewService {

  COURSE_URL = "/api/api/course/"
  constructor(private _http: HttpClient) { }

  listRegisteredCourses() {
    return this._http.get(`${this.COURSE_URL}/registerlist`)
  }

  listRegisteredEvents() {
    return this._http.get(`http://localhost:3000/api/event/registerlist`)
  }

}
