import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  EVENT_URL = "/api/api/event/"
  constructor(private _http: HttpClient) { }

  listEvents() {
    return this._http.get(`${this.EVENT_URL}/list`)
  }
  getEvent(id: any) {
    return this._http.get(`${this.EVENT_URL}/get/${id}`)
  }

  addEvent(data: any) {
    return this._http.post(`${this.EVENT_URL}/add`, data)
  }

  registerEvent(data: any) {
    return this._http.post(`${this.EVENT_URL}/register`, data)

  }

  deleteEvent(id: any) {
    return this._http.get(`${this.EVENT_URL}/remove/${id}`)
  }
}
