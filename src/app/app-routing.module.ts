import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { AdminDashComponent } from './admin-dash/admin-dash.component';
import { BodyComponent } from './body/body.component';
import { CoursesComponent } from './courses/courses.component'
import { EventsComponent } from './events/events.component';
import { LoginComponent } from './login/login.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { RegisterCourseComponent } from './register-course/register-course.component';
import { ContactComponent } from './contact/contact.component';
import { RegisterEventComponent } from './register-event/register-event.component';
import { AddCourseComponent } from './add-course/add-course.component';
import { AdminGuard } from './admin.guard';
import { AdminCourseComponent } from './admin-course/admin-course.component';
import { EditCourseComponent } from './edit-course/edit-course.component';
import { LogoutComponent } from './logout/logout.component';
import { DeleteCourseComponent } from './delete-course/delete-course.component';
import { DeleteCourseRegComponent } from './delete-course-reg/delete-course-reg.component';
import { AdmineventComponent } from './adminevent/adminevent.component';
import { AddEventComponent } from './add-event/add-event.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import { DeleteEventComponent } from './delete-event/delete-event.component';


const routes: Routes = [
  { path: '', component: BodyComponent },
  { path: 'course', component: CoursesComponent },
  { path: 'events', component: EventsComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registercourse/:courseid', component: RegisterCourseComponent },
  { path: 'addcourse', component: AddCourseComponent, canActivate: [AdminGuard] },
  { path: 'addevent', component: AddEventComponent, canActivate: [AdminGuard] },
  { path: 'editcourse/:courseid', component: EditCourseComponent, canActivate: [AdminGuard] },
  { path: 'editevent/:courseid', component: EditEventComponent, canActivate: [AdminGuard] },
  { path: 'removecourse/:courseid', component: DeleteCourseComponent, canActivate: [AdminGuard] },
  { path: 'removeevent/:courseid', component: DeleteEventComponent, canActivate: [AdminGuard] },
  { path: 'removecoursereg/:courseid', component: DeleteCourseRegComponent, canActivate: [AdminGuard] },
  { path: 'logout', component: LogoutComponent, canActivate: [AdminGuard] },
  { path: 'registerevent/:courseid', component: RegisterEventComponent },
  { path: 'contact', component: ContactComponent },
  {
    path: 'admin', component: AdminDashComponent, canActivate: [AdminGuard], children: [
      { path: 'course', component: AdminCourseComponent },
      { path: 'event', component: AdmineventComponent }

    ]
  }
  ,
  //Wild Card Route for 404 request
  {
    path: '**', pathMatch: 'full',
    component: NotFoundPageComponent
  },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
