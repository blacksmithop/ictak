import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.scss']
})
export class EditCourseComponent implements OnInit {
  reader = new FileReader();
  file: any;
  name = 'Description';
  htmlContent = '';


  RegisterForm: FormGroup = this.fb.group(
    {
      title: ['', Validators.required],
      category: ['', Validators.required],
      description: ['', Validators.required],
      criteria: ['', Validators.required],
      fee: ['', Validators.required],
      duration: ['', Validators.required],
      internship_partner: ['', Validators.required],
    });

  constructor(private api: CourseService,
    private router: Router,
    private fb: FormBuilder,

    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.api.getCourse(this.route.snapshot.params.courseid).subscribe((data: any) => {
      delete data['__v'];
      delete data['_id'];
      delete data['image_url'];
      console.log(data);

      for (let key in data) {
        this.RegisterForm.get(key)!.setValue(data[key]);
      }
    })
  }


  fileChangeEvent(e: any) {
    this.file = e.target.files[0];
    this.reader.readAsDataURL(this.file);
    this.reader.onload = (_event) => {
      this.file = this.reader.result;
    }
  }

  prefill() {

  }

  checkDetails() {
    let form = this.RegisterForm.value;
    form.description = form.description.replace(/<[^>]*>/g, '');
    form['_id'] = this.route.snapshot.params.courseid;

    form.image_url = this.file
    console.log(form);
    this.api.editCourse(form) // id here
      .subscribe(
        (response: any) => {
          let result = response;
          console.log(result);
        })
    this.router.navigate(['admin/course']);
  }

}
