import { Component, OnInit } from '@angular/core';
import { AdminviewService } from '../adminview.service';
import { CourseService } from '../course.service';

@Component({
  selector: 'app-admin-course',
  templateUrl: './admin-course.component.html',
  styleUrls: ['./admin-course.component.scss']
})
export class AdminCourseComponent implements OnInit {
  registeredCourses: any;
  listCourses: any;
  constructor(private api: AdminviewService,
    private course: CourseService) {
  }

  ngOnInit(): void {

    this.course.listCourses().subscribe(
      (resp: any) => {
        console.log(resp)
        this.listCourses = resp;
      });

    this.api.listRegisteredCourses().subscribe(
      (resp: any) => {
        console.log(resp)
        this.registeredCourses = resp;
      }
    );
  }

}
