import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../event.service';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.scss']
})
export class EditEventComponent implements OnInit {
  reader = new FileReader();
  file: any;
  name = 'Description';
  htmlContent = '';

  RegisterForm: FormGroup = this.fb.group(
    {
      name: ['', Validators.required],
      organizer: ['', Validators.required],
      description: ['', Validators.required],
      fee: ['', Validators.required],
      duration: ['', Validators.required],
      contact: ['', Validators.required],
    });

  constructor(private api: EventService,
    private router: Router,
    private fb: FormBuilder,

    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.api.getEvent(this.route.snapshot.params.courseid).subscribe((data: any) => {
      console.log(data);
      delete data['__v'];
      delete data['_id'];
      delete data['image_url'];
      console.log(data);

      for (let key in data) {
        this.RegisterForm.get(key)!.setValue(data[key]);
      }
    })
  }


  fileChangeEvent(e: any) {

    this.file = e.target.files[0];
    this.reader.readAsDataURL(this.file);
    this.reader.onload = (_event) => {
      this.file = this.reader.result;
      console.log(this.file);
    }
  }

  checkDetails() {
    let form = this.RegisterForm.value;
    console.log(form);
    form.image_url = this.file

    this.api.addEvent(form)
      .subscribe(
        (response: any) => {
          let result = response;
          console.log(result);
        })
    this.router.navigate(['admin/event']);
  }

}
